import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler

# 710997779:AAHNSLa1Qd5CokHnBiC-LXNFIaDYUKdkRbI
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!"
    )


if __name__ == "__main__":
    application = (
        ApplicationBuilder()
        .token("710997779:AAHNSLa1Qd5CokHnBiC-LXNFIaDYUKdkRbI")
        .build()
    )

    start_handler = CommandHandler("start", start)
    application.add_handler(start_handler)

    application.run_polling()

# https://api.telegram.org/bot123456789:jbd78sadvbdy63d37gda37bd8/getUpdates
